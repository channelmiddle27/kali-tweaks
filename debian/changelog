kali-tweaks (2023.3.2) kali-dev; urgency=medium

  * SSH config: use drop-in file provided by kali-defaults

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 13 Jul 2023 15:12:44 +0700

kali-tweaks (2023.3.1) kali-dev; urgency=medium

  * Fix xrdp audio module install for hyper-v

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 06 Jun 2023 11:18:01 +0700

kali-tweaks (2023.2.2) kali-dev; urgency=medium

  * Catch exceptions in the metapackages screen (thx X0RW3LL)
  * Don't fail if there are warnings during apt update (thx X0RW3LL)

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 25 Apr 2023 11:04:16 +0700

kali-tweaks (2023.2.1) kali-dev; urgency=medium

  * Don't call systemctl if systemd is not running

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 05 Apr 2023 16:43:16 +0700

kali-tweaks (2023.1.5) kali-dev; urgency=medium

  * Default to blue color for informative messages

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 03 Mar 2023 20:49:26 +0700

kali-tweaks (2023.1.4) kali-dev; urgency=medium

  * Update for non-free-firmware

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 27 Feb 2023 20:30:52 +0700

kali-tweaks (2023.1.3) kali-dev; urgency=medium

  * Add a message of caution for developers of the SSH setting
  * Enable legacy MACs in SSH wide compat

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 26 Jan 2023 15:54:01 +0700

kali-tweaks (2023.1.2) kali-dev; urgency=medium

  * Add a link to the kernel settings documentation
  * Don't use '+' signs in SSH config
  * Define some very basic regression tests
  * Drop the license_file key in setup.cfg
  * Filter out the salsa additive version from the VERSION file

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 17 Jan 2023 22:16:42 +0700

kali-tweaks (2023.1.1) kali-dev; urgency=medium

  [ Arnaud Rebillout ]
  * Update package description
  * Update version constraint on kali-defaults
  * install_pkgs: Make the '-y' configurable
  * Add dummy function documentation for pkgs install/remove
  * Add 'apt_' prefix to functions '{install,remove}_pkgs'
  * Always run apt update before checking metapackages
  * Ask confirmation before installing metapackages
  * Try harder to do the right thing (TM) when installing metapackages
  * Move metapackage helpers in utils.py
  * Move the pkg/desc adjustments where it belongs
  * Make the button labels in confirm window configurable
  * Use Skip instead of Cancel to skip apt upgrade
  * Move the call to apt_update within apt_list_kali_metapackages
  * Avoid calling apt update all the time
  * Another take on apt update's optimization

  [ X0RW3LL ]
  * Fix minor typo to reflect privileged ports < 1024

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 20 Dec 2022 23:00:34 +0700

kali-tweaks (2023.1.0) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * Add d/watch

  [ Arnaud Rebillout ]
  * Update tox.ini, as flake8 and black never seem to agree
  * Fix a likely bug when writing a list to file
  * Add sysctl settings (dmesg restrict and privileged ports)

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 16 Dec 2022 16:06:53 +0700

kali-tweaks (2022.4.1) kali-dev; urgency=medium

  * Add workaround for broken testparms

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 27 Oct 2022 08:57:54 +0700

kali-tweaks (2022.4.0) kali-dev; urgency=medium

  * Update polkit rules for Hyper-V enhanced session mode
  * Update Standards-Version (no change required)

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 21 Oct 2022 10:51:42 +0700

kali-tweaks (2022.3.0) kali-dev; urgency=medium

  * Add support for OpenSSL providers (openssl 3.x)
  * Minor reword of the OpenSSL setting description

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 13 Jul 2022 18:24:37 +0200

kali-tweaks (2022.2.1) kali-dev; urgency=medium

  * Drop '--configfile' from Samba's testparm

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 29 Apr 2022 09:06:16 +0700

kali-tweaks (2022.2.0) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 04 Mar 2022 09:27:47 +0700

kali-tweaks (2022.1.3) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 15 Feb 2022 13:29:36 +0700

kali-tweaks (2022.1.2) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 24 Jan 2022 10:47:22 +0700

kali-tweaks (2022.1.1) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 19 Jan 2022 10:00:01 +0700

kali-tweaks (2022.1.0) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 19 Jan 2022 08:53:21 +0700

kali-tweaks (2021.4.0) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 25 Nov 2021 08:37:44 +0700

kali-tweaks (2021.3.3) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 09 Sep 2021 15:12:59 +0700

kali-tweaks (2021.3.2) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 06 Sep 2021 09:17:55 +0700

kali-tweaks (2021.3.1) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 26 Aug 2021 01:03:03 +0700

kali-tweaks (2021.3.0) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Sun, 22 Aug 2021 01:11:46 +0700

kali-tweaks (2021.2.2) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 21 May 2021 15:39:39 +0700

kali-tweaks (2021.2.1) kali-dev; urgency=medium

  * New release.

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 19 May 2021 21:21:09 +0700

kali-tweaks (2021.2.0) kali-dev; urgency=medium

  * Initial release

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 11 May 2021 13:56:23 +0700
