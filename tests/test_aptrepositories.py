# Copyright 2021 Offensive Security
# SPDX-license-identifier: GPL-3.0-only

import unittest
from urllib.parse import urlparse

from kali_tweaks.settings.aptrepositories import (
    Source,
    parse_kali_source,
    parse_sources_list,
    print_kali_source,
    update_sources_list,
)


class TestAptRepositoriesParse(unittest.TestCase):
    def assert_parse(self, line, expected, **kwargs):
        source = parse_kali_source(line, **kwargs)
        self.assertEqual(source, expected)

    def test_common_cases(self):
        components = "main contrib non-free non-free-firmware"
        for mirror in ["http.kali.org", "kali.download"]:
            url = f"http://{mirror}/kali"
            src = f"deb {url} kali-rolling {components}"
            exp = Source("deb", None, urlparse(url), "kali-rolling", components)
            self.assert_parse(src, exp)

    def test_deb_src(self):
        url = "http://http.kali.org/kali"
        src = f"deb-src {url} kali-dev main"
        exp = Source("deb-src", None, urlparse(url), "kali-dev", "main")
        self.assert_parse(src, exp)
        self.assert_parse(src, None, discard_src=True)

    def test_with_options(self):
        url = "http://http.kali.org/kali"
        src = f"deb [opt1=foo] {url} kali-dev main"
        exp = Source("deb", "[opt1=foo]", urlparse(url), "kali-dev", "main")
        self.assert_parse(src, exp)
        src = f"deb [ opt1=foo, opt2=bar ] {url} kali-dev main"
        exp = Source("deb", "[ opt1=foo, opt2=bar ]", urlparse(url), "kali-dev", "main")
        self.assert_parse(src, exp)

    def test_third_party_mirror(self):
        url = "http://third.party.mirror.com/Linux/kali"
        src = f"deb {url} kali-dev main"
        exp = Source("deb", None, urlparse(url), "kali-dev", "main")
        self.assert_parse(src, exp)

    def test_cdrom(self):
        url = "cdrom://http.kali.org/kali"
        src = f"deb {url} kali-dev main"
        exp = Source("deb", None, urlparse(url), "kali-dev", "main")
        self.assert_parse(src, exp)

    def test_invalid_suite(self):
        url = "http://http.kali.org/kali"
        src = f"deb {url} this-is-not-kali main"
        exp = None
        self.assert_parse(src, exp)

    def test_components(self):
        components = "main contrib non-free"
        url = "http://http.kali.org/kali"
        src = f"deb {url} kali-dev {components}"
        exp = Source("deb", None, urlparse(url), "kali-dev", components)
        self.assert_parse(src, exp)
        components = "main contrib non-free non-free-firmware"
        src = f"deb {url} kali-dev {components}"
        exp = Source("deb", None, urlparse(url), "kali-dev", components)
        self.assert_parse(src, exp)

    def test_empty_line(self):
        self.assert_parse("", None)
        self.assert_parse("\n\n\n", None)

    def test_commented_line(self):
        src = "#deb http://http.kali.org/kali kali-dev main"
        self.assert_parse(src, None)

    def test_malformed_lines(self):
        src = "foobar http://http.kali.org/kali kali-dev main"
        with self.assertRaises(ValueError):
            self.assert_parse(src, None)
        src = "deb [opt1=foo http://http.kali.org/kali kali-dev main"
        with self.assertRaises(ValueError):
            self.assert_parse(src, None)
        src = "deb http://http.kali.org/kali kali-dev"
        with self.assertRaises(ValueError):
            self.assert_parse(src, None)


class TestAptRepositoriesPrint(unittest.TestCase):
    def assert_print(self, source, expected):
        line = print_kali_source(source)
        self.assertEqual(line, expected)

    def test_common_cases(self):
        components = "main contrib non-free non-free-firmware"
        for mirror in ["http.kali.org", "kali.download"]:
            url = f"http://{mirror}/kali"
            src = Source("deb", None, urlparse(url), "kali-rolling", components)
            exp = f"deb {url} kali-rolling {components}"
            self.assert_print(src, exp)


class TestAptRepositoriesParseSourcesList(unittest.TestCase):
    def assert_parse(self, content, expected, **kwargs):
        sources = parse_sources_list(content, **kwargs)
        self.assertEqual(sources, expected)

    def test_one_source(self):
        url = "http://http.kali.org/kali"
        components = "main contrib non-free non-free-firmware"
        content = f"deb {url} kali-rolling {components}"
        exp = [Source("deb", None, urlparse(url), "kali-rolling", components)]
        self.assert_parse(content, exp)

    def test_multiple_sources_with_comments(self):
        url = "http://kali.download/kali"
        components = "main contrib non-free non-free-firmware"
        content = f"""
## kali rolling, with sources ##
deb     {url} kali-rolling {components}
deb-src {url} kali-rolling {components}
## kali exp, no sources ##
deb     {url} kali-experimental {components}
"""
        exp = [
            Source("deb", None, urlparse(url), "kali-rolling", components),
            Source("deb-src", None, urlparse(url), "kali-rolling", components),
            Source("deb", None, urlparse(url), "kali-experimental", components),
        ]
        self.assert_parse(content, exp)
        exp = [
            Source("deb", None, urlparse(url), "kali-rolling", components),
            Source("deb", None, urlparse(url), "kali-experimental", components),
        ]
        self.assert_parse(content, exp, discard_src=True)


class TestAptRepositoriesUpdateSourcesList(unittest.TestCase):
    def assert_update(self, content, expected, **kwargs):
        res = update_sources_list(content, **kwargs)
        self.assertEqual(res, expected)

    def test_protocol(self):
        old = "deb http://http.kali.org/kali kali-dev main"
        new = "deb https://http.kali.org/kali kali-dev main\n"
        self.assert_update(old, (old, False), protocol="http")
        self.assert_update(old, (new, True), protocol="https")

    def test_mirror(self):
        old = "deb http://http.kali.org/kali kali-dev main"
        new = "deb http://kali.download/kali kali-dev main\n"
        self.assert_update(old, (old, False), mirror="http.kali.org")
        self.assert_update(old, (new, True), mirror="kali.download")

    def test_protocol_plus_mirror(self):
        old = "deb http://http.kali.org/kali kali-dev main"
        new = "deb https://kali.download/kali kali-dev main\n"
        self.assert_update(old, (new, True), mirror="kali.download", protocol="https")

    def test_remove_suite(self):
        old = "deb http://http.kali.org/kali kali-dev main"
        new = ""
        self.assert_update(old, (old, False), remove_suites=["foo"])
        self.assert_update(old, (new, True), remove_suites=["kali-dev"])

    def test_non_kali_source(self):
        old = "deb http://http.kali.org/kali not-kali main"
        self.assert_update(old, (old, False), protocol="https")

    def test_all_at_once(self):
        old = """
# This is a bit surprising
deb http://foo.bar/abcd not-kali hence not modified
deb http://foo.bar/whatever kali-dev this is a kali source
# This is just the usual
deb http://http.kali.org/kali kali-rolling main contrib non-free non-free-firmware
deb-src http://http.kali.org/kali kali-rolling main contrib
deb http://http.kali.org/kali kali-bleeding-edge main contrib non-free non-free-firmware
deb-src http://http.kali.org/kali kali-bleeding-edge main contrib
"""
        new = """
# This is a bit surprising
deb http://foo.bar/abcd not-kali hence not modified
deb https://kali.download/kali kali-dev this is a kali source
# This is just the usual
deb https://kali.download/kali kali-rolling main contrib non-free non-free-firmware
deb-src https://kali.download/kali kali-rolling main contrib
"""
        self.assert_update(
            old,
            (new, True),
            mirror="kali.download",
            protocol="https",
            remove_suites=["kali-bleeding-edge"],
        )


if __name__ == "__main__":
    unittest.main()
